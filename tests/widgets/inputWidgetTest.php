<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/widgetTest.php';


abstract class Widget_InputWidgetTest extends Widget_WidgetTest
{
    protected $itemClass = 'Widget_InputWidget';
    
    
    /**
     * The html string returned by the display() method must contain the item attribute name and value.
     */
    public function testInputWidgetInDisplayModeHasCorrespondingClassInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
        
        $item->setDisplayMode();
        
        $W = bab_Widgets();
        
        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);
        

        
        
        /*
         * 
        $matcher = array(
            'attributes' => array(
                'class' => 'widget-displaymode'
            )
        );
        
        $this->assertTag(
            $matcher,
            $html,
            'The html class "widget-displaymode" was not present for a display mode inputWidget ' . $this->itemClass
        );
        */
        
        $xpathQueryResult = $this->getXPathMatchClass($html, 'widget-displaymode');
        $this->assertEquals(1, $xpathQueryResult->length );
        
        
    }
    
    
    
    protected function getHtmlName($inputName)
    {
        if (is_array($inputName)) {
            $inputName = Widget_HtmlCanvas::getHtmlName($inputName);
        }
        
        return $inputName;
    }


    /**
     * The html string returned by the display() method must contain the inputWidget name.
     */
    public function testSimpleNameIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
    
        $name = 'myName';
        $item->setName($name);
    
        $W = bab_Widgets();
        $canvas = $W->HtmlCanvas();
        
        $html = $item->display($canvas);
        /*
        $matcher = array('attributes' => array('name' => $this->getHtmlName($name)));
        $this->assertTag(
            $matcher,
            $html,
            'There were no name attribute matching the item name for ' . $this->itemClass
        );
        */
        
        $xpathQueryResult = $this->getXPathSearchAttribute($html, 'name', $this->getHtmlName($name));
        $this->assertEquals( 1, $xpathQueryResult->length, 'There were no name attribute matching the item name for ' . $this->itemClass );
    }
        
    /**
     * The html string returned by the display() method must contain the inputWidget name.
     */
    public function testMultipleNameIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
        
        $name = array('myContact', 'name');
        $item->setName($name);
        
        $W = bab_Widgets();
        $canvas = $W->HtmlCanvas();
        
        $html = $item->display($canvas);
        /*
        $matcher = array('attributes' => array('name' => $this->getHtmlName($name)));
        $this->assertTag(
            $matcher,
            $html,
            'There were no name attribute matching the full item name for ' . $this->itemClass
        );
        */
        $xpathQueryResult = $this->getXPathSearchAttribute($html, 'name', $this->getHtmlName($name));
        $this->assertEquals( 1, $xpathQueryResult->length, 'There were no name attribute matching the full item name for ' . $this->itemClass );
    }
}
