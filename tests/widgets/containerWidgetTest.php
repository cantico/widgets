<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/widgetTest.php';

abstract class Widget_ContainerWidgetTest extends Widget_WidgetTest
{
    protected $itemClass = 'Widget_ContainerWidget';
    
    
    /**
     * When an item is added to a container, the item getParent()
     * must return the container.
     */
    public function testItemAddedToContainerHasContainerAsParent()
    {
        $W = bab_Widgets();
        
        // Creates a Mock_Widget_Item.
        $container = $this->construct();

        $layout = $W->Layout();

        $container->setLayout($layout);
        
        $lineEdit = $W->LineEdit();
    
        $container->addItem($lineEdit);
        
        $lineEditParent = $lineEdit->getParent();

        $this->assertSame(
            $lineEditParent,
            $container,
            'The parent of item added to container is not the container (' . $this->itemClass . ')'
        );
    }
    
    
    /**
     *
     */
    public function testDumpingContainerContainsContainedItemDump()
    {
        $W = bab_Widgets();
        
        // Creates a Mock_Widget_Item.
        $container = $this->construct();

        $layout = $W->Layout();

        $container->setLayout($layout);
        
        $lineEdit = $W->LineEdit();
    
        $container->addItem($lineEdit);

        $lineEditDump = $lineEdit->dump();
        $containerDump = $container->dump();
                
        $this->assertContains(
            $lineEditDump,
            $containerDump
        );
    }
}
