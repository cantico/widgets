<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/frameTest.php';

class Widget_MultiFieldTest extends Widget_FrameTest
{
    protected $itemClass = 'Widget_MultiField';

    /**
     * @return Widget_Item
     */
    protected function construct()
    {
        $item = parent::construct();
        $item->setName('test');
        return $item;
    }
}
