<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/inputwidget.class.php';





/**
 * A Widget_DelayedItem is a placeholder item which can have its content automatically
 * updated through ajax.
 *
 *
 */
class Widget_DelayedItem extends Widget_Item implements Widget_Displayable_Interface
{
    /**
     * Update mode causing the delayed item being automatically updated when it
     * becomes visible on the user's screen.
	 */
	const UPDATE_MODE_ON_APPEAR = 'on-appear';

	/**
     * Update mode causing the delayed item being automatically updated when the
     * page finishes loading.
	 */
	const UPDATE_MODE_ON_PAGE_LOAD = 'on-page-load';

	/**
	 * Update mode causing the delayed item not being automatically updated.
	 */
	const UPDATE_MODE_NONE = 'none';


	protected $placeHolderItem = null;
	protected $updateMode = Widget_DelayedItem::UPDATE_MODE_ON_APPEAR;


	/**
	 * @param Widget_Action $delayedAction An action returning a widget.
	 * @param string        $id            The item unique id.
	 * @return Widget_InputWidget
	 */
	public function __construct(Widget_Action $delayedAction = null, $id = null)
	{
		parent::__construct($id);
		if (isset($delayedAction))
		{
			$this->setDelayedAction($delayedAction);
		}
	}


	/**
	 * Sets the action that will be called when the delayed item has
	 * to be refreshed.
	 *
	 * @param Widget_Action $delayedAction
	 *
	 * @return Widget_DelayedItem
	 */
	public function setDelayedAction(Widget_Action $delayedAction)
	{
		return $this->setReloadAction($delayedAction);
	}


	/**
	 * Returns the action that will be called when the delayed item has
	 * to be refreshed.
	 *
	 * @return Widget_Action
	 */
	public function getDelayedAction()
	{
		return $this->getReloadAction();
	}


	/**
	 * Sets the item that will be displayed before the delayed item is
	 * updated.
	 *
	 * @param Widget_Displayable_Interface $item
	 *
	 * @return Widget_DelayedItem
	 */
	public function setPlaceHolderItem(Widget_Displayable_Interface $item = null)
	{
		$this->placeHolderItem = $item;
		return $this;
	}


	/**
	 * Returns the item that will be displayed before the delayed item is
	 * updated.
	 *
	 * @return Widget_Item
	 */
	public function getPlaceHolderItem()
	{
		return $this->placeHolderItem;
	}


	/**
	 * Defines the way the delayed item content will be updated.
	 *
	 * @param string $mode	One of the Widget_DelayedItem::UPDATE_MODE_xxx constants.
	 *
	 * @return Widget_DelayedItem
	 */
	public function setUpdateMode($mode = Widget_DelayedItem::UPDATE_MODE_ON_APPEAR)
	{
		$this->updateMode = $mode;
		return $this;
	}


	/**
	 * Returns the way the delayed item content will be updated.
	 * Should be one of the Widget_DelayedItem::UPDATE_MODE_xxx constants.
	 *
	 * @return string
	 */
	public function getUpdateMode()
	{
		return $this->updateMode;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_InputWidget#getClasses()
	 *
	 * @return array
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-delayeditem';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		if ($this->getDelayedAction()) {
			$this->setMetadata('delayedAction', $this->getDelayedAction()->url());
		}
		if ($this->getUpdateMode()) {
			$this->setMetadata('updateMode', $this->getUpdateMode());
		}

		$displayableInterface = array();
		if ($this->getPlaceHolderItem()) {
			$displayableInterface = array($this->getPlaceHolderItem());
		}

		return $canvas->div(
			$this->getId(),
			$this->getClasses(),
			$displayableInterface
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}

}
