<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';




/**
 * Constructs a Widget_CheckBox.
 *
 * @param string $id			The item unique id.
 * @return Widget_CheckBox
 */
function Widget_CheckBox($id = null)
{
	return new Widget_CheckBox($id);
}


/**
 * A Widget_Checkbox is a widget that let the user enter a boolean value (on/off).
 *
 */
class Widget_CheckBox extends Widget_InputWidget implements Widget_Displayable_Interface
{

	private $checked_value = '1';
	private $unchecked_value = '0';


	/**
	 * @param string $id			The item unique id.
	 * @return Widget_CheckBox
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-checkbox';
		return $classes;
	}

	/**
	 * Sets the value that will be submitted if the checkbox is checked.
	 *
	 * @param	string	$value
	 * @return self
	 */
	public function setCheckedValue($value)
	{
		$this->checked_value = $value;
		return $this;
	}

	/**
	 * Sets the value that will be submitted if the checkbox is not checked.
	 * If null, no value is submitted if the checkbox is not checked.
	 *
	 * @param	string|null  $value
	 * @return self
	 */
	public function setUncheckedValue($value)
	{
		$this->unchecked_value = $value;
		return $this;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::setValue()
	 */
	public function setValue($value)
	{
		assert('is_bool($value) || $value === $this->checked_value || $value === $this->unchecked_value; /* The "value" parameter must be a boolean or one of the values defined by setCheckedValue and setUncheckedValue. */');
		if (is_bool($value)) {
			$value = ($value ? $this->checked_value : $this->unchecked_value);
		}
		return parent::setValue($value);
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Item::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		if ($this->isDisplayMode()) {
		    $this->addClass('widget-displaymode');
			$this->disable();
		}


		return $canvas->checkBoxInput(
			$this->getId(),
			$this->getClasses(),
			$this->getFullName(),
			$this->getValue(),
			$this->checked_value,
			$this->unchecked_value,
			$this->isDisabled(),
			$this->getTitle(),
		    $this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
