<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/inputwidget.class.php';


/**
 * Constructs a Widget_LineEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_LineEdit
 */
function Widget_LineEdit($id = null)
{
    return new Widget_LineEdit($id);
}



/**
 * Used by Widget_LineEdit and Widget_SuggestLineEdit
 * the object with this interface have a widget-lineedit css class
 */
interface Widget_LineEdit_Interface
{


    /**
     * Sets the visible size of the line edit .
     *
     * @param int $size
     */
    public function setSize($size);


    /**
     * Returns the visible size (in characters) of the line edit.
     *
     * @return int
     */
    public function getSize();


    /**
     * Sets the readonly parameter .
     *
     * @param bool $value
     * @return self
     */
    public function setReadOnly($value);


    /**
     * Returns the value of the readonly parameter.
     *
     * @return bool
     */
    public function isReadOnly();


    /**
     * @deprecated
     * @see Widget_LineEdit_Interface::isReadOnly()
     * @return bool
     */
    public function getReadOnly();


    /**
     * Sets the maximum input size (in characters) of the line edit.
     *
     * @param int $maxSize
     * @return self
     */
    public function setMaxSize($maxSize);

    /**
     * Returns the maximum input size (in characters) of the line edit.
     *
     * @return int
     */
    public function getMaxSize();
}


/**
 * A Widget_LineEdit is a widget that lets the user enter a single line of text.
 */
class Widget_LineEdit extends Widget_InputWidget implements Widget_Displayable_Interface, Widget_LineEdit_Interface
{
    /**
     * @var int
     */
    private $_size;

    /**
     * @var int
     */
    private $_maxSize;

    /**
     * @var bool
     */
    private $_readOnly = false;

    /**
     * @var bool
     */
    private $_obfuscated;

    /**
     * @var bool
     */
    private $_autoComplete = true;


    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->_size = null;
        $this->_maxSize = null;
        $this->_obfuscated = false;
    }


    /**
     * Sets the visible size of the line edit .
     *
     * @param int $size
     * @return self
     */
    public function setSize($size)
    {
        assert('is_int($size); /* The "size" parameter must be an integer */');
        $this->_size = $size;
        return $this;
    }


    /**
     * Returns the visible size (in characters) of the line edit.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->_size;
    }


    /**
     * Sets the readonly parameter .
     *
     * @param bool $value
     * @return self
     */
    public function setReadOnly($value)
    {
        assert('is_bool($value); /* The "value" parameter must be a boolean */');
        $this->_readOnly = $value;
        return $this;
    }


    /**
     * Returns the value of the readonly parameter.
     *
     * @return bool
     */
    public function isReadOnly()
    {
        return $this->_readOnly;
    }


    /**
     * @deprecated
     * @see Widget_LineEdit::isReadOnly()
     * @return bool
     */
    public function getReadOnly()
    {
        return $this->_readOnly;
    }

    /**
     * Sets the maximum input size (in characters) of the line edit.
     *
     * @param int $maxSize
     * @return self
     */
    public function setMaxSize($maxSize)
    {
        assert('is_int($maxSize);  /* The "maxSize" parameter must be an integer */');
        $this->_maxSize = $maxSize;
        return $this;
    }

    /**
     * Returns the maximum input size (in characters) of the line edit.
     *
     * @return int
     */
    public function getMaxSize()
    {
        return $this->_maxSize;
    }


    /**
     * Sets the autocomplete attribute of the line edit.
     *
     * When autocomplete in true, the web browser will try to fill the line edit
     * with stored values (for login and password fields for example) or propose
     * a list of previously entered values.
     *
     * @param bool	$value
     * @return self
     */
    public function setAutoComplete($value)
    {
        assert('is_bool($value); /* The "value" parameter must be a boolean */');
        $this->_autoComplete = $value;
        return $this;
    }


    /**
     * Returns the autocomplete attribute value of the line edit.
     *
     * @see programs/widgets/Widget_LineEdit::setAutoComplete()
     *
     * @return bool
     */
    public function getAutoComplete()
    {
        return $this->_autoComplete;
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-lineedit';
        return $classes;
    }


    /**
     * Sets/unsets the field as obfuscated, i.e. the content
     * is not displayed to the user, as in a password field.
     *
     * @param bool	$obfuscate		True to make the field obfuscated.
     * @return self
     */
    public function obfuscate($obfuscate = true)
    {
        $this->_obfuscated = $obfuscate;
        return $this;
    }

    /**
     * Checks whether the field is obfuscated, i.e. the content
     * is not displayed to the user, as in a password field.
     *
     * @return bool		True if the field is obfuscated.
     */
    public function isObfuscated()
    {
        return $this->_obfuscated;
    }


    protected function getInputType()
    {
        return 'text';
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $maxSize = $this->getMaxSize();
        if (isset($maxSize)) {
            $value = substr($this->getValue(), 0, $maxSize);
        } else {
            $value = $this->getValue();
        }


        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            if ($this->getSize()) {
                $options = $this->getCanvasOptions();
                if (is_null($options)) {
                    $options = $this->Options();
                }
                $options->minWidth(1.4 * $this->getSize(), 'ex');
                $this->setCanvasOptions($options);
            }

            return $canvas->richtext(
                $this->getId(),
                $classes,
                $this->getValue(),
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            ) . $canvas->metadata($this->getId(), $this->getMetadata());


        } else {

            if ($this->_obfuscated) {
                return $canvas->passwordInput(
                    $this->getId(),
                    $this->getClasses(),
                    $this->getFullName(),
                    $value,
                    $this->getSize(),
                    $this->getMaxSize(),
                    $this->isDisabled(),
                    $this->getTitle(),
                    $this->isReadOnly(),
                    $this->getAutoComplete(),
                    $this->getPlaceHolder()
                )
                . $canvas->metadata($this->getId(), $this->getMetadata());
            } else {
                return $canvas->lineInput(
                    $this->getId(),
                    $this->getClasses(),
                    $this->getFullName(),
                    $value,
                    $this->getSize(),
                    $this->getMaxSize(),
                    $this->isDisabled(),
                    $this->isReadOnly(),
                    $this->getAutoComplete(),
                    $this->getInputType(),
                    $this->getPlaceHolder(),
                    $this->getCanvasOptions(),
                    $this->getTitle(),
                    $this->getAttributes()
                )
                . $canvas->metadata($this->getId(), $this->getMetadata());
            }
        }
    }
}
