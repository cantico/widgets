<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once FUNC_WIDGETS_PHP_PATH . 'widget.class.php';


/**
 * Constructs a Widget_Gauge.
 *
 * @param string		$id			The item unique id.
 * @return Widget_Gauge
 */
function Widget_Gauge($id = null)
{
	return new Widget_Gauge($id);
}


/**
 * A Widget_Gauge is a widget that display a gauge
 */
class Widget_Gauge extends Widget_Widget implements Widget_Displayable_Interface
{
	/**
	 * progress in percent
	 * @var int
	 */
	private $progress = 50;


	/**
	 * @param string $id			The item unique id.
	 * @return Widget_Gauge
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
	}

	/**
	 * Set gauge progession
	 * @param	int	$progress
	 * @return Widget_Gauge
	 */
	public function setProgress($progress)
	{
		$this->progress = (int) $progress;
		return $this;
	}


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getClasses()
     */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-gauge';
		return $classes;
	}

	/**
	 * Get html ID of the progress div
	 * @return string
	 */
	public function getProgressId()
	{
		return $this->getId().'_progress';
	}


	/**
	 * @param Widget_Canvas    $canvas
	 * @param string           $id
	 * @param string[]         $classes
	 */
	protected function gauge($canvas, $id, $classes)
	{
		$progressOptions = $this->Options();
		$progressOptions->width($this->progress, '%');


		$frame = $canvas->div($id,
							$classes,
							array($canvas->div(
								$this->getProgressId(),
								array(),
								array(),
								$progressOptions
							)),
							$this->getCanvasOptions()
		);

		return $frame;
	}


	/**
	 * {@inheritDoc}
	 * @see Widget_Item::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
	    $addon = bab_getAddonInfosInstance('widgets');


		$frame = $this->gauge($canvas, $this->getId(), $this->getClasses());
		$frame .= $canvas->metadata($this->getId(), $this->getMetadata());
		$frame .= $canvas->loadStyleSheet($addon->getStylePath().'widgets.gauge.css');

		return $frame;
	}

}
