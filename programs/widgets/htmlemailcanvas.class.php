<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
include_once dirname(__FILE__).'/htmlcanvas.class.php';


/**
 * Constructs a Widget_HtmlEmailCanvas
 *
 * @return Widget_HtmlEmailCanvas
 */
function Widget_HtmlEmailCanvas() {
    return new Widget_HtmlEmailCanvas;
}

/**
 * This Canvas will generate HTML strings without scripts.
 *
 */
class Widget_HtmlEmailCanvas extends Widget_HtmlCanvas
{
    
    
    /**
     * output url for link in the canvas
     * If this is a relative url, add the domain
     * @param string $url
     * @return string
     */
    protected function outputUrl($url)
    {
        if (0 !== mb_strpos($url, 'http')) {
            return bab_getBabUrl().$url;
        }
        
        return url;
    }
    


    /**
     * Create metadata container
     *
     * @param	string	$name
     * @param	array	$metadata
     * @return string
     */
    public function metadata($name, $metadata = null)
    {
        return '';
    }


    /**
     * Load script
     * @param	string	$widget_id
     * @param	string	$filename
     * @return string
     */
    public function loadScript($widget_id, $filename) {

        return '';
    }


    /**
     * Load CSS style sheet
     * @param	string	$filename
     * @return string
     */
    public function loadStyleSheet($filename) {

        return '';
    }
}