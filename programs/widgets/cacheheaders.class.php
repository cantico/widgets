<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';



/**
 * Cache headers associated to a Widget_Item
 * the cache headers are reported into parent item if the item have a parent
 *
 */
class Widget_CacheHeaders
{
	/**
	 * Disable cache explicitly
	 * @var bool
	 */
	private $disabled = false;
	
	/**
	 * possible cache duration for this item
	 * @var int		seconds
	 */
	private $ttl = null;
	
	
	/**
	 * Last modifificiation date
	 * @var BAB_DateTime
	 */
	private $lastModified = null;
	
	
	/**
	 * A string used to compose the etag
	 * @var string
	 */
	private $etagComponent = array();
	
	/**
	 * @var string
	 */
	private $etag;
	
	
	/**
	 * set the cache control method of the response
	 * @var bool
	 */
	private $isPublic = null;
	
	
	/**
	 * disable cache control explicitly
	 * @return Widget_CacheHeaders
	 */
	public function disable()
	{
		$this->disabled = true;
		return $this;
	}
	
	
	/**
	 *
	 * @return Widget_CacheHeaders
	 */
	public function setPublic()
	{
		$this->isPublic = true;
		return $this;
	}
	
	/**
	 * time to live in cache (seconds)
	 * @param int $ttl
	 * @return Widget_CacheHeaders
	 */
	public function setTTL($ttl)
	{
		if (isset($this->ttl) && $this->ttl < $ttl) {
			// ignore, current ttl is lower than the new ttl
			return $this;
		}
		
		$this->ttl = $ttl;
		return $this;
	}
	
	
	/**
	 * Set last modification date
	 * @param	BAB_DateTime	$lastModified
	 * @return Widget_CacheHeaders
	 */
	public function setLastModified(BAB_DateTime $lastModified)
	{
		if (isset($this->lastModified) && $this->lastModified->getIsoDateTime() > $lastModified->getIsoDateTime()) {
			// ignore, current last modified date is closest
			return $this;
		}
		
		$this->lastModified = $lastModified;
		return $this;
	}
	
	
	
	/**
	 * Set Etag component
	 * @param	string	$str
	 * @return Widget_CacheHeaders
	 */
	public function setEtagComponent($str)
	{
		if (null === $this->etagComponent) {
			throw new ErrorException('The Etag has allready been used');
		}
		
		if (!is_string($str)) {
			throw new ErrorException('Etag component must be a string');
		}
		
		$this->etagComponent[] = $str;
		return $this;
	}
	
	
	/**
	 *
	 * @return string
	 */
	public function getEtag()
	{
		if (null === $this->etag) {
			if (empty($this->etagComponent)) {
				return null;
			}
			
			$this->etag = md5(implode('/', $this->etagComponent));
			$this->etagComponent = null;
		}
		return $this->etag;
	}
	
	
	/**
	 * Combine with another cacheHeaders object
	 * @param	Widget_CacheHeaders $cacheHeaders
	 * @return $this;
	 */
	public function combineWith(Widget_CacheHeaders $cacheHeaders)
	{
		if (null !== $cacheHeaders->ttl) {
			$this->setTTL($cacheHeaders->ttl);
		}
		
		if (isset($cacheHeaders->lastModified)) {
			$this->setLastModified($cacheHeaders->lastModified);
		}
		
		$this->etagComponent += $cacheHeaders->etagComponent;
		
		return $this;
	}
	
	
	/**
	 * Get the requested cache control parameters
	 * @return array
	 */
	public function getCacheControl()
	{
		$request = array();
		
		if (isset($_SERVER['HTTP_CACHE_CONTROL'])) {
		
			foreach (explode(',', $_SERVER['HTTP_CACHE_CONTROL']) as $prop) {
				$arr = explode('=', $prop);
				if (isset($arr[0])) {
					$property = trim($arr[0]);
					$value = null;
					if (isset($arr[1])) {
						$value = trim($arr[1]);
						if (is_numeric($value)) {
							$value = (int) $value;
						}
					}
		
					$request[$property] = $value;
				}
			}
		}
		
		return $request;
	}
	
	
	

	
	/**
	 * @return string
	 */
	private function getCacheControlMethod()
	{
		if (isset($this->isPublic)) {
			return $this->isPublic ? 'public' : 'private';
		}
		
		return bab_isUserLogged() ? 'private' : 'public';
	}
	
	
	/**
	 * Test if request match the last modification date
	 * @return bool
	 */
	private function requestMatchLastModified()
	{
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
			if ($_SERVER['HTTP_IF_MODIFIED_SINCE'] === $this->lastModified->getHttp()) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Test if request match Etag
	 * @return bool
	 */
	private function requestMatchEtag()
	{
		if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
			if ($_SERVER['HTTP_IF_NONE_MATCH'] === $this->getEtag()) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Test if the user do a manual refresh
	 * @return bool
	 */
	private function requestMustRevalidate()
	{
		if ($request = cachecontrol_getRequest()) {
			if ((isset($request['max-age']) && 0 == $request['max-age']) || isset($request['no-cache']) || isset($request['must-revalidate'])) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Set no cache headers in response
	 */
	private function setMustRevalidate()
	{
		$ts = gmdate("D, d M Y H:i:s") . " GMT";
		header("Expires: $ts");
		header("Last-Modified: $ts");
		header("Pragma: no-cache");
		header("Cache-Control: no-cache, must-revalidate");
	}
	
	
	/**
	 * Set the last-modified header in response if possible
	 */
	private function setHeaderLastModified()
	{
		if (null === $this->lastModified) {
			return;
		}
		
		$lastmodified = $this->lastModified->getHttp();
		
		header("Last-Modified: $lastmodified");
	}
	
	
	/**
	 * Set the expires header in response if possible
	 */
	private function setHeaderExpires()
	{
		if (null === $this->ttl) {
			$this->header_remove('Expires');
			return;
		}
		
		$ts = gmdate("D, d M Y H:i:s", time() + $this->ttl) . " GMT";
		header("Expires: $ts");
	}
	
	/**
	 * Set the Etag header in response if possible
	 *
	 */
	private function setHeaderEtag()
	{
		if (null === $etag = $this->getEtag()) {
			return;
		}
		
		header("ETag: $etag");
	}
	
	
	/**
	 * Set the Cache-Control header in response
	 *
	 */
	private function setHeaderCacheControl()
	{
		$value = $this->getCacheControlMethod();
		
		if (null !== $this->ttl) {
			$value .= ', max-age='.$this->ttl;
		}
		
		header("Cache-Control: $value");
	}
	
	
	/**
	 * Set the not modified header in response
	 *
	 */
	private function setNotModified()
	{
		header("HTTP/1.1 304 Not Modified");
		$this->setHeaderExpires();
		$this->setHeaderLastModified();
		$this->setHeaderCacheControl();
		$this->setHeaderEtag();
		exit;
	}
	
	
	private function header_remove($name)
	{
		if (function_exists('header_remove')) {
			// php > 5.3.0
			header_remove($name);
		} else {
			header("$name:"); // remove the default Pragma header?
		}
	}
	
	
	/**
	 * send headers, exit if cache is usable
	 */
	public function send()
	{
		if ($this->requestMustRevalidate()) {
			$this->setMustRevalidate();
			return;
		}
		
		if ($this->requestMatchLastModified() || $this->requestMatchEtag()) {
			$this->setNotModified();
		}
		
		
		$this->setHeaderExpires();
		$this->setHeaderLastModified();
		$this->setHeaderCacheControl();
		$this->setHeaderEtag();
		
		$this->header_remove('Pragma');
	}
}
