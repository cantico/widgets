<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';






/**
 * Constructs a Widget_SuggestUser.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestUser
 */
function Widget_SuggestUser($id = null)
{
	return new Widget_SuggestUser($id);
}


/**
 * A Widget_SuggestUser is a widget that let the user found a user with entry.
 * Propose user suggestions based on database
 */
class Widget_SuggestUser extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{
    /**
     * @param string $id			The item unique id.
     * @return Widget_LineEdit
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->setMetadata('suggesturl', array('addon' => 'widgets.suggest', 'idx' => 'user'));

        $this->setMetadata('suggestparam', 'search');
    }


    /**
     * {@inheritDoc}
     * @see Widget_SuggestEdit::setIdValue()
     */
    public function setIdValue($id)
    {
        $name = bab_getUserName($id, true);
        $this->setValue($name);

        return parent::setIdValue($id);
    }


    /**
     * {@inheritDoc}
     * @see Widget_InputWidget::setValue()
     */
    public function setValue($value)
    {
        if (is_int($value)) {
            return $this->setIdValue($value);
        }

        parent::setValue($value);

        return $this;
    }

    /**
     * Send suggestions
     */
    public function suggest()
    {
        if (false !== $keyword = $this->getSearchKeyword()) {

            $babDB = bab_getDB();

            $searchPattern = '%' . str_replace(' ', '', $keyword) . '%';
            $sql = 'SELECT id, nickname, lastname, firstname
                FROM ' . BAB_USERS_TBL . '
                WHERE
                    TRIM(CONCAT(firstname, lastname)) LIKE ' . $babDB->quote($searchPattern) . '
                OR
                    TRIM(CONCAT(lastname, firstname)) LIKE ' . $babDB->quote($searchPattern) . '
                LIMIT ' . Widget_SuggestLineEdit::MAX;
            $users = $babDB->db_query($sql);

            $i = 0;
            if ($babDB->db_num_rows($users) == 0) {
                $this->addSuggestion('', widget_translate('No user match'));
            } else {
                while ($user = $babDB->db_fetch_assoc($users)) {
                    $i++;
                    if ($i > Widget_SuggestLineEdit::MAX) {
                        break;
                    }

                    $fullname = bab_composeUserName($user['firstname'], $user['lastname']);
                    $this->addSuggestion(
                        $user['id'],
                        $fullname,
                        $user['nickname']
                    );
                }
            }

            $this->sendSuggestions();
        }
    }


    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
    	$classes = parent::getClasses();
    	$classes[] = 'widget-suggestuser';
    	return $classes;
    }
}