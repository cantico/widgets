<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__).'/lineedit.class.php';


/**
 * Constructs a Widget_DrilldownMenu.
 *
 * @param string		$id			The item unique id.
 * @return Widget_DrilldownMenu
 */
function Widget_DrilldownMenu($id = null)
{
	return new Widget_DrilldownMenu($id);
}


/**
 * A Widget_DrilldownMenu is a widget that let the user chose an item in a tree
 *
 *
 */
class Widget_DrilldownMenu extends Widget_LineEdit implements Widget_Displayable_Interface
{



	/**
	 * @param string $id			The item unique id.
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);

		$this->setMetadata('topLinkText', widget_translate('All'));
		$this->setMetadata('crumbDefaultText', widget_translate('Choose an option'));
	}



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-drilldown-menu';

		return $classes;
	}



	/**
	 * Set url of datasource
	 * the target page contain a UL LI tree with links for selectable items
	 * href content will be set in the input field
	 * tag content will be displayed as selectable item
	 *
	 * @param	bab_Url		$datasource
	 * @return Widget_DrilldownMenu
	 */
	public function setDatasource(bab_Url $datasource) {

		$this->setMetadata('datasource', $datasource->toString());

		return $this;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_LineEdit#display($canvas)
	 */
	public function display(Widget_Canvas $canvas)
	{
		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			return $canvas->span(
					$this->getId(),
					$classes,
					array($canvas->text($this->getTitle())),
					$this->getCanvasOptions()
			);
		}


		$link = $canvas->linkContainer(
			null,
			array('widget-drilldown-menu-placeholder', 'widget-drilldown-menu-placeholder-icon-right', 'ui-widget',  'ui-state-default', 'ui-corner-all'),
			array($canvas->span(null, array('ui-icon', 'ui-icon-triangle-1-s'), array()), $canvas->span(null, array('text'), array($canvas->text($this->getTitle())))),
			$this->getMetadata('datasource'),
			$this->getCanvasOptions()
		);

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		return $link.parent::display($canvas)
			.$canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.drilldownmenu.jquery.js')
			.$canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.drilldownmenu.css');
	}
}