<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/lineedit.class.php';



/**
 * Constructs a Widget_RegExpLineEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_RegExpLineEdit
 */
function Widget_RegExpLineEdit($id = null)
{
	return new Widget_RegExpLineEdit($id);
}


/**
 * A Widget_RegExpLineEdit is a widget that let the user enter a single line of text with a regular expression check.
 *
 */
class Widget_RegExpLineEdit extends Widget_LineEdit implements Widget_Displayable_Interface
{
	private $regExp;

	/* French date format */
	const DATE_FORMAT = '0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d';

	/* Float format */
	const FLOAT_FORMAT = '^[-+]?[0-9]*[\.\,]?[0-9]+$';

	/* Signed int format */
	const INT_FORMAT = '^[-+]?[0-9]+$';

	/* Unsigned int format */
	const UINT_FORMAT = '^[0-9]+$';

	/* Email format */
	const EMAIL_FORMAT = '^\b[A-Za-z0-9.-]*[A-Za-z0-9]@[A-Za-z0-9.-]+\.[A-Za-z]{2,7}\b$';



	/**
	 * @param string $id			The item unique id.
	 * @return Widget_RegExpLineEdit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->regExp = '';
	}



	/**
	 * Sets the regular expression used to validate the line edit.
	 *
	 * @param string $regExp
	 */
	public function setRegExp($regExp)
	{
		$this->regExp = $regExp;
		$this->setMetadata('regExp', $regExp);
		return $this;
	}



	/**
	 * Returns the regular expression used to validate the line edit.
	 *
	 * @return string
	 */
	public function getRegExp()
	{
		return $this->regExp;
	}




	/**
	 * Message displayed on form submit if there is a regexp line edit widget not matching the regular expression.
	 *
	 * @param string	$str
	 * @return Widget_RegExpLineEdit
	 */
	public function setSubmitMessage($str)
	{
		$this->setMetadata('submitMessage', $str);
		return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see Widget_LineEdit::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-regexplineedit';

		return $classes;
	}



	/**
	 * (non-PHPdoc)
	 * @see Widget_LineEdit::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$message = $this->getMetadata('submitMessage');
		if (!$this->isMandatory() && empty($message)) {
			$message .= " \n" . widget_translate('Do you want to submit the form anyway?');
			$this->setSubmitMessage($message);
		}

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		$output = parent::display($canvas)
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.regexplineedit.jquery.js');

		return $output;
	}
}
