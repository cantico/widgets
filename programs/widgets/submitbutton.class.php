<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/inputwidget.class.php';
require_once dirname(__FILE__) . '/action.class.php';


/**
 * Constructs a Widget_SubmitButton.
 *
 * @param string $id			The item unique id.
 * @return Widget_SubmitButton
 */
function Widget_SubmitButton($id = null)
{
	return new Widget_SubmitButton($id);
}



/**
 * This widget is used to submit its parent form.
 */
class Widget_SubmitButton extends Widget_InputWidget implements Widget_Displayable_Interface
{

	/**
	 * The button text label.
	 *
	 * @var string $_label
	 */
	private $_label = null;

	/**
	 * The button action.
	 *
	 * @var Widget_Action $_action
	 */
	private $_action = null;

	/**
	 * @var Widget_Action
	 */
	private $successAction = null;

	/**
	 * @var Widget_Action
	 */
	private $failedAction = null;



	/**
	 * The button title
	 *
	 */
	private $_title = null;


	const VALIDATE = 1;
	const MANDATORY = 2;


	/**
	 * @var int
	 */
	private $validate = 0;

	/**
	 * @param string $id			The item unique id.
	 * @return $this
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
	}

	/**
	 * Sets the button text label.
	 *
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		$this->_label = $label;

		return $this;
	}

	/**
	 * Do nothing, you should use setLabel($label) to set the the text on button
	 *
	 * @param string $value
	 * @return $this
	 */
	public function setValue($value)
	{
		bab_debug("You should use setLabel(\$label) to set the text on button");
		return $this;
	}

	/**
	 * Returns the button text label.
	 *
	 * @return string
	 */
	public function getLabel()
	{
		return $this->_label;
	}


	/**
	 * Sets the action.
	 *
	 * @param Widget_Action $action
	 * @return $this
	 */
	public function setAction(Widget_Action $action = null)
	{
		$this->_action = $action;
		
		return $this;
	}

	/**
	 * Returns the action.
	 *
	 * @return Widget_Action
	 */
	public function getAction()
	{
		return $this->_action;
	}

	/**
	 * Sets the action to use if the page defined in setAction method is successfull.
	 *
	 * @param Widget_Action $action
	 * @return $this
	 */
	public function setSuccessAction(Widget_Action $action = null)
	{
		$this->successAction = $action;
		return $this;
	}

	/**
	 * Sets the action to use if the page defined in setAction method failed.
	 *
	 * @param Widget_Action $action
	 * @return $this
	 */
	public function setFailedAction(Widget_Action $action = null)
	{
		$this->failedAction = $action;
		return $this;
	}



	/**
	 * Forces validation of the containing form before submitting data.
	 *
	 * @see Widget_InputWidget::setMandatory()
	 * @param 	int	$validate		Widget_SubmitButton::VALIDATE to validate fields data before submiting (only mandatory fields are mandatory, other verification diplayed with a confirm dialog)
	 * 								Widget_SubmitButton::MANDATORY to force all fields valid on form before submiting
	 * @return $this
	 */
	public function validate($validate = Widget_SubmitButton::VALIDATE)
	{
		$this->validate = $validate;
		return $this;
	}



	/**
	 * set a confirmation message displayed in a modal dialog before submit
	 * @param	string	$message
	 * @return 	$this
	 */
	public function setConfirmationMessage($message)
	{
		$this->setMetadata('confirmationMessage', $message);
		$this->addClass('widget-confirm');

		return $this;
	}



	/**
	 * Set open mode, choose the opening method of the link
	 * @param	int	$mode
	 *
	 * @return self
	 */
	public function setOpenMode($mode)
	{
	    switch ($mode) {
	        case Widget_Link::OPEN_POPUP:
	            $this->addClass('widget-popup');
	            break;
	    }

	    return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-submitbutton';
		$classes[] = 'ui-default-state';

		switch($this->validate) {

			case self::VALIDATE:
				$classes[] = 'widget-submitbutton-validate';
			break;

			case self::MANDATORY:
				$classes[] = 'widget-submitbutton-mandatory';
				break;
		}

		return $classes;
	}




	/**
	 * (non-PHPdoc)
	 * @see Widget_Item::display()
	 */
	function display(Widget_Canvas $canvas)
	{
		if ($this->isDisplayMode()) {
		    $classes = $this->getClasses();
		    $classes[] = 'widget-displaymode';
			$ouput = $canvas->span(
    			$this->getId(),
    			$classes,
    			array(),
    			$this->getCanvasOptions(),
    			'',
    			$this->getAttributes()
    		) . $canvas->metadata($this->getId(), $this->getMetadata());
			return $ouput;
		}

		$action = $this->getAction();

		if (null === $action) {
			$fullname = $this->getFullName();
		} else {
			$fullname = $action->getFullName();
		}
		
		
		$form = $this->getForm();
        if ($form && isset($action)) {
    		if (!$form->isReadOnly() && !$action->isSaveMethodRequired() && !$action->isDeleteMethodRequired()) {
    		    bab_debug(
    		        sprintf(
    		            'Submit button %s will submit on an unprotected action %s', 
    		            $this->getId(),
    		            $action->getMethod()
    		        ),
    		        DBG_ERROR,
    		        'CSRF'
    		    );
    		}
        }
		

		$ouput = $canvas->submitInput(
			$this->getId(),
			$this->getClasses(),
			$fullname,
			$this->getLabel(),
			$this->isDisabled(),
			$this->getTitle(),
			$this->getAttributes()
		) . $canvas->metadata($this->getId(), $this->getMetadata());

		if (isset($this->successAction)) {
			$success = $fullname;
			array_shift($success);
			array_unshift($success, '_ctrl_success');
			$ouput .= $canvas->hidden(null, array(), $success, $this->successAction->url());
		}

		if (isset($this->failedAction)) {
			$failed = $fullname;
			array_shift($failed);
			array_unshift($failed, '_ctrl_failed');
			$ouput .= $canvas->hidden(null, array(), $failed, $this->failedAction->url());
		}

		return $ouput;
	}
}
