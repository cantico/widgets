<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
require_once dirname(__FILE__) . '/textedit.class.php';
require_once dirname(__FILE__) . '/suggestedit.class.php';

/**
 * Constructs a Widget_SuggestTextEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestTextEdit
 */
function Widget_SuggestTextEdit($id = null)
{
	return new Widget_SuggestTextEdit($id);
}


/**
 * A Widget_SuggestTextEdit is a widget that let the user enter a muliple lines of text and proposes
 * suggestions as the user types.
 */
class Widget_SuggestTextEdit extends Widget_SuggestEdit implements Widget_Displayable_Interface, Widget_TextEdit_Interface
{
	private $_nbColumns = 40;
	private $_nbLines = 5;
	private $_maxSize = null;
	private $_maxWords = null;

    /**
     * Sets the visible width of the text edit.
     *
     * @param int $nbColumns	The number of visible columns.
     *
     * @return self
     */
	public function setColumns($nbColumns)
	{
		assert('is_int($nbColumns); /* The "nbColumns" parameter must be an integer */');
		$this->_nbColumns = $nbColumns;
		return $this;
	}

	/**
	 * Returns the visible width (in characters) of the text edit.
	 *
	 *
	 */
	public function getColumns()
	{
		return $this->_nbColumns;
	}


	/**
	 * Sets the vertical input size (in characters) of the text edit.
	 *
	 * @param int $nbLines		The number of visible lines.
	 *
	 * @return self
	 */
	public function setLines($nbLines)
	{
		assert('is_int($nbLines); /* The "nbLines" parameter must be an integer */');
		$this->_nbLines = $nbLines;
		return $this;
	}

	/**
	 * Returns the vertical input size (in characters) of the text edit.
	 *
	 * @return int
	 */
	public function getLines()
	{
		return $this->_nbLines;
	}


	/**
	 * Sets the maximum input size (in characters) of the line edit.
	 *
	 * @param int $maxSize
	 * @return self
	 */
	public function setMaxSize($maxSize)
	{
		assert('is_int($maxSize);  /* The "maxSize" parameter must be an integer */');
		$this->_maxSize = $maxSize;
		return $this;
	}

	/**
	 * Returns the maximum input size (in characters) of the line edit.
	 *
	 * @return int
	 */
	public function getMaxSize()
	{
		return $this->_maxSize;
	}




	/**
	 * Sets the maximum input size (in characters) of the line edit.
	 *
	 * @param int $maxWords
	 * @return self
	 */
	public function setMaxWords($maxWords)
	{
	    assert('is_int($maxWords);  /* The "maxWords" parameter must be an integer */');
	    $this->_maxWords = $maxWords;
	    return $this;
	}

	/**
	 * Returns the maximum input size (in characters) of the line edit.
	 *
	 * @return int
	 */
	public function getMaxWords()
	{
	    return $this->_maxWords;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_LineEdit#getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggesttextedit';
		$classes[] = 'widget-textedit';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_LineEdit#display($canvas)
	 */
	public function display(Widget_Canvas $canvas) {

		$output = parent::display($canvas);

	if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			if ($this->getColumns()) {
				$options = $this->getCanvasOptions();
				if (is_null($options)) {
					$options = $this->Options();
				}
				$options->minWidth(1.4 * $this->getColumns(), 'ex');
				$this->setCanvasOptions($options);
			}

			$output .= $canvas->richtext(
				$this->getId(),
				$classes,
				$this->getValue(),
				BAB_HTML_ALL,
				$this->getCanvasOptions()
			) . $canvas->metadata($this->getId(), $this->getMetadata());


		} else {

			if (isset($this->_maxSize)) {
				$this->setMetadata('maxSize', $this->getMaxSize());
			}

			$output .= $canvas->textInput($this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$this->getValue(),
				$this->getColumns(),
				$this->getLines(),
				$this->isDisabled(),
			    $this->getTitle(),
                $this->getAttributes()
			) . $canvas->metadata($this->getId(), $this->getMetadata());

		}

		return $output;
	}
}
