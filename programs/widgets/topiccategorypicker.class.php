<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';


/**
 * Constructs a Widget_TopicCategoryPicker.
 *
 * @param string        $id         The item unique id.
 * @return Widget_TopicCategoryPicker
 */
function Widget_TopicCategoryPicker($id = null)
{
    return new Widget_TopicCategoryPicker($id);
}


/**
 * A Widget_TopicCategoryPicker is a widget that let the user select an ovidentia category.
 *
 * It currently opens a popup window displaying a topic treeview with clickable nodes.
 */
class Widget_TopicCategoryPicker extends Widget_InputWidget implements Widget_Displayable_Interface
{

    /**
     * @param string $id            The item unique id.
     * @return Widget_TopicCategoryPicker
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }



    /**
     * (non-PHPdoc)
     * @see Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        if (!$this->isDisplayMode()) {
            $classes[] = 'widget-topiccategorypicker';
            $classes[] = 'icon';
            $classes[] = 'apps-articles';
        }
        return $classes;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
        $value = $this->getValue();

        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            return $canvas->richtext(
                $this->getId(),
                $classes,
                bab_getTopicCategoryTitle($value),
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            );

        }

        if ($value !== '') {
            $this->setMetadata('categoryName', bab_getTopicCategoryTitle($value));
        } else {
            $this->setMetadata('categoryName', '');
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');


        return $canvas->span(
            $this->getId(),
            $this->getClasses(),
            array(
                $canvas->lineInput(
                    $this->getId().'input',
                    array(),
                    $this->getFullName(),
                    $value,
                    10,
                    10,
                    $this->isDisabled(),
                    false,
                    false,
                    'text',
                    $this->getPlaceHolder(),
                    null,
                    null,
                    null
                )
            ),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.topicpicker.jquery.js');
    }
}
