<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once dirname(__FILE__) . '/image.class.php';


/**
 * Constructs a Widget_ColorPicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_ColorPicker
 */
function Widget_ImageCropper($id = null)
{
    return new Widget_ImageCropper($id);
}


/**
 * A Widget_ColorPicker is a widget that let the user enter a color hexadecimal code.
 * or pick a color throw a dialog
 */
class Widget_ImageCropper extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

    private	$minW = 0;
    private	$minH = 0;
    private	$minSelectedW = 0;
    private	$minSelectedH = 0;
    private	$ratio = 0;
    private	$x = null;
    private	$y = null;
    private	$width = null;
    private	$height = null;
    private	$rotate = null;
    private	$inputLayout = null;
    private	$imageWidget = null;
    private	$visibleLayout = true;

    /**
     * @param string $id			The item unique id.
     * @return Widget_ImageCropper
     */
    public function __construct($id = null)
    {
        $W = bab_Widgets();

//         require_once FUNC_WIDGETS_PHP_PATH . 'flowlayout.class.php';
//         require_once FUNC_WIDGETS_PHP_PATH . 'lineedit.class.php';
//         require_once FUNC_WIDGETS_PHP_PATH . 'image.class.php';
//         require_once FUNC_WIDGETS_PHP_PATH . 'namedcontainer.class.php';
//         require_once FUNC_WIDGETS_PHP_PATH . 'hidden.class.php';

        bab_Functionality::get('Icons');

        $layout = $W->VBoxLayout($id);
        $this->inputLayout = $W->NamedContainer();

        parent::__construct($id, $layout);

        $this->x 		= $W->LineEdit($this->getId().'_x');
        $this->y 		= $W->LineEdit($this->getId().'_y');
        $this->width 	= $W->LineEdit($this->getId().'_width');
        $this->height 	= $W->LineEdit($this->getId().'_height');
        $this->rotate 	= $W->LineEdit($this->getId().'_rotate');

        $this->x->setName('x')->setMetadata('x', $this->x->getId())->setSize(5);
        $this->y->setName('y')->setMetadata('y', $this->y->getId())->setSize(5);
        $this->width->setName('width')->setMetadata('width', $this->width->getId())->setSize(5);
        $this->height->setName('height')->setMetadata('height', $this->height->getId())->setSize(5);
        $this->rotate->setName('rotate')->setMetadata('rotate', $this->rotate->getId())->setSize(5);

        $this->imageWidget = $W->Image();
        $this->imageWidget->setId($this->getId().'_image');

        $layout->addItem(
            $this->imageWidget->addClass('widget-imagecropper')
        )->addItem(
            $W->FlowLayout($this->getId().'_buttonlayout')->setHorizontalSpacing(20)->addClass(Func_Icons::ICON_TOP_16)->addItem(
                $W->Button(
                    $this->getId().'_rotateleft'
                )->addItem(
                    $W->VBoxItems()->addClass('icon',Func_Icons::ACTIONS_GO_PREVIOUS)
                )->setTitle(widget_translate('Rotate Left'))
            )
            ->addItem(
                $W->Button(
                    $this->getId().'_rotateright'
                )->addItem(
                    $W->VBoxItems()->addClass('icon',Func_Icons::ACTIONS_GO_NEXT)
                )->setTitle(widget_translate('Rotate Right'))
            )
        )->addItem(
            $this->inputLayout->addItem(
                $W->FlowLayout($this->getId().'_inputlayout')->setHorizontalSpacing(20)->addItem(
                    $W->FlowLayout()
                        ->addItem($this->xLabel = $W->Label(widget_translate('x'). chr(160)))
                        ->addItem($this->x->setAssociatedLabel($this->xLabel)
                    )->setVerticalAlign('middle')
                )->addItem(
                    $W->FlowLayout()
                        ->addItem($this->yLabel = $W->Label(widget_translate('y'). chr(160)))
                        ->addItem($this->y->setAssociatedLabel($this->yLabel)
                    )->setVerticalAlign('middle')
                )->addItem(
                    $W->FlowLayout()
                        ->addItem($this->widthLabel = $W->Label(widget_translate('width'). chr(160)))
                        ->addItem($this->width->setAssociatedLabel($this->widthLabel)
                    )->setVerticalAlign('middle')
                )->addItem(
                    $W->FlowLayout()
                        ->addItem($this->heightLabel = $W->Label(widget_translate('height'). chr(160)))
                        ->addItem($this->height->setAssociatedLabel($this->heightLabel)
                    )->setVerticalAlign('middle')
                )->addItem(
                    $W->FlowLayout()
                        ->addItem($this->rotateLabel = $W->Label(widget_translate('rotate'). chr(160)))
                        ->addItem($this->rotate->setAssociatedLabel($this->rotateLabel)
                    )->setVerticalAlign('middle')
                )
            )
        )->setVerticalSpacing(5, 'px');
    }

//     /**
//     * Sets the name of input.
//     *
//     * @param string	$name
//     * @return Widget_ImageCropper
//     */
//     public function setName($name)
//     {
//         $this->inputLayout->setName($name);

//         return $this;
//     }

    /**
     * Sets the image url.
     *
     * @param string	$url
     * @return Widget_ImageCropper
     */
    public function setUrl($url)
    {
        $this->imageWidget->setUrl($url);
        return $this;
    }

    /**
     * Sets the max image cropping size.
     *
     * @param int	$w
     * @param int	$h
     * @return Widget_ImageCropper
     */
    /*public function setMaxSize($w, $h)
    {
        $this->maxW = $w;
        $this->maxH = $h;
        return $this;
    }*/

    /**
     * Sets the min image cropping size.
     *
     * @param int	$w
     * @param int	$h
     * @return Widget_ImageCropper
     */
    public function setMinSize($w, $h)
    {
        $this->minW = $w;
        $this->minH = $h;
        return $this;
    }

    /**
     * Sets the min image cropping size.
     *
     * @param int	$w
     * @param int	$h
     * @return Widget_ImageCropper
     */
    public function setMinSelectedSize($w, $h)
    {
        $this->minSelectedW = $w;
        $this->minSelectedH = $h;
        return $this;
    }

    /**
     * Sets the image cropping ratio.
     *
     * @param float	$ratio
     * @return Widget_ImageCropper
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
        return $this;
    }

    /**
     * Sets the image input hidden.
     *
     * @param float	$visible
     * @return Widget_ImageCropper
     */
    public function setVisibleLayout($visible = true)
    {
        $this->visibleLayout = $visible;
        return $this;
    }

    /**
    * Sets the image cropping selection.
    *
    * @param int	$x
    * @param int	$y
    * @param int	$width
    * @param int	$height
    * @param int	$rotate
    * @return Widget_ImageCropper
    */
    public function setValue( $x, $y, $width, $height, $rotate = 0)
    {
        $this->x->setValue($x);
        $this->y->setValue($x);
        $this->width->setValue($width);
        $this->height->setValue($height);
        $this->rotate->setValue($rotate);

        return $this;
    }


    /**
     * @see Widget_Item::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget_imagecropper_container';

        return $classes;
    }

    public function display(Widget_Canvas $canvas)
    {
        $this->imageWidget->setMetadata('minSizeW', $this->minW);
        $this->imageWidget->setMetadata('minSizeH', $this->minH);
        $this->imageWidget->setMetadata('minSelectedW', $this->minSelectedW);
        $this->imageWidget->setMetadata('minSelectedH', $this->minSelectedH);
        $this->imageWidget->setMetadata('aspectRatio', $this->ratio);
        $this->imageWidget->setMetadata('x', $this->x->getValue());
        $this->imageWidget->setMetadata('y', $this->x->getValue());
        $this->imageWidget->setMetadata('width', $this->width->getValue());
        $this->imageWidget->setMetadata('height', $this->height->getValue());
        $this->imageWidget->setMetadata('rotate', $this->rotate->getValue());
        $this->imageWidget->setMetadata('visibleInputs', $this->visibleLayout);

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $imageCropperDiv = $canvas->div(
            $this->getId(),
            $this->getClasses(),
            $this->getLayout(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.imagecropper.jquery.js')
        . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.imagecropper.css');

        return $imageCropperDiv;
    }

}
