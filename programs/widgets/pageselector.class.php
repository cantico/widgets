<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';



/**
 * Constructs a Widget_PageSelector.
 *
 * @param string    $id     The item unique id.
 * @return Widget_PageSelector
 */
function Widget_PageSelector($id = null)
{
    return new Widget_PageSelector($id);
}



/**
 * Widget_PageSelector
 *
 */
class Widget_PageSelector extends Widget_Widget implements Widget_Displayable_Interface
{
    /**
     * @var CountableIterator | Array
     */
    private $iterator;

    /**
     * @var int
     */
    private $pageLength = 20;

    /**
     * First page is 0.
     * @var int
     */
    private $currentPage = 0;

    /**
     * @var int
     */
    private $maxLinks = 15;


    private $pageNamePath = null;

    private $sortField = null;

    /**
     *
     * @var string
     */
    private $anchorname = null;


    /**
     * Initialized as POST + GET parameters.
     * @var array
     */
    private $gp;

    /**
     * A reference to the page number variable in gp.
     * @var int
     */
    private $pageNumberGp;

    /**
     * @param string $id    The item unique id.
     * @return Widget_Label
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

    }

    /**
     *
     * @param CountableIterator | Array  $iterator
     * @return Widget_PageSelector
     */
    public function setIterator($iterator)
    {
        $this->iterator = $iterator;
        return $this;
    }



    /**
     * Sets the maximum number of rows that shall be displayed at once.
     *
     * @param int $pageLength       or null to unset the limit.
     * @return Widget_PageSelector
     */
    public function setPageLength($pageLength = null)
    {
        $this->pageLength = $pageLength;
        return $this;
    }

    /**
     * Returns the maximum number of rows that shall be displayed at once.
     *
     * @return int      The maximum number of rows displayed or null if no limit.
     */
    public function getPageLength()
    {
        return $this->pageLength;
    }


    /**
     * Sets the currently displayed page. DIsplayed data depends on pageLength.
     *
     * @param $pageNumber       First page is 0.
     * @return Widget_PageSelector
     */
    public function setCurrentPage($pageNumber)
    {
        $this->currentPage = $pageNumber;
        return $this;
    }


    /**
     * Returns the currently displayed page.
     *
     * @return int      The currently displayed page. First page is 0.
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }




    /**
     * Returns the number of pages necessary to display all the data.
     *
     * @return int  The number of pages necessary to display all the data or null if impossible to determine.
     */
    public function getNbPages()
    {
        if (!isset($this->pageLength)) {
            return 1;
        }
        if ($this->iterator instanceof Countable && $this->pageLength > 0) {
            return ceil($this->iterator->count() / $this->pageLength);
        }

        if (is_array($this->iterator) && $this->pageLength > 0) {
            return ceil(count($this->iterator) / $this->pageLength);
        }

        return null;
    }


    /**
     * Set page name path
     * @param array $path
     * @return Widget_PageSelector
     */
    public function setPageNamePath(array $path)
    {
        $this->pageNamePath = $path;
        return $this;
    }

    /**
     * Get page name path
     * @return array
     */
    public function getPageNamePath()
    {
        if (null === $this->pageNamePath) {
            return $this->getNamePath();
        }

        return $this->pageNamePath;
    }



    /**
     * Defines the data source field that will be used to perform sorting.
     *
     * @param string $fieldPath
     * @return Widget_PageSelector
     */
    public function setSortField($fieldPath)
    {
        $this->sortField = $fieldPath;
        return $this;
    }


    /**
     * Set an anchor for destination page
     * @param string $anchorname
     * @return Widget_PageSelector
     */
    public function setAnchor($anchorname)
    {
        $this->anchorname = $anchorname;
        return $this;
    }


    /**
     * Get the anchor name of destination page
     * @return string | null
     */
    public function getAnchor()
    {
        return $this->anchorname;
    }



    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     *
     * @return mixed
     */
    protected static function getRecordFieldValue(ORM_Record $record, $fieldPath)
    {
        $fieldPathElements = explode('/', $fieldPath);
        $value = $record;
        $field = $record->getParentSet();
        foreach ($fieldPathElements as $fieldName) {
            if (!$field->fieldExist($fieldName)) {
                return null;
            }
            $field = $field->$fieldName;
            $value = $value->$fieldName;
        }

        return $field->output($value);
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        //$classes[] = 'widget-pageselector';
        $classes[] = 'pagination';
        return $classes;
    }





    /**
     *
     * @return Widget_Select
     */
    protected function dropdownPageSelector(Widget_Canvas $canvas)
    {
        $iterator = null;
        $nbRows = null;

        if (isset($this->sortField)) {
            if ($this->iterator instanceof Iterator)
            {
                $iterator = $this->iterator;
            }
            $nbRows = $iterator->count();
        }

        $options = array();
        for ($i = 0; $i < $this->getNbPages(); $i++) {
            if (isset($iterator)) {
                $iterator->seek($i * $this->getPageLength());
                $record = $iterator->current();
                $pageStartValue = mb_convert_case(mb_substr(self::getRecordFieldValue($record, $this->sortField), 0, 5), MB_CASE_UPPER);
                $iterator->seek(min($nbRows - 1, ($i + 1) * $this->getPageLength() - 1));
                $record = $iterator->current();
                $pageEndValue = mb_convert_case(mb_substr(self::getRecordFieldValue($record, $this->sortField), 0, 5), MB_CASE_UPPER);
                $options[$i] = sprintf(widget_translate('Page %d : %s to %s'), ($i + 1), $pageStartValue, $pageEndValue);

            } else {
                $options[$i] = 'Page ' . ($i + 1);
            }
        }
        $pageSelector = $canvas->select(null, array(), $this->getPageNamePath(), $this->getCurrentPage(), $options, array(), false);
        return $pageSelector;
    }



    /**
     * @param   array   multidimentional array for query parameters
     * @return unknown_type
     */
    private function pageSelectorUrl($page)
    {
        $this->pageNumberGp = $page;

        $currentUrl = parse_url($_SERVER['REQUEST_URI']);

        $url =  $currentUrl['path']. '?' . bab_url::buildQuery($this->gp);

        if (isset($this->anchorname)) {
            $url .= '#'.urlencode($this->anchorname);
        }

        return $url;
    }


    /**
     *
     * @param CountableIterator|Array $iterator
     * @param int                     $nbRows
     * @param int                     $pageNumber
     * @return string
     */
    protected function getPageLabel($iterator = null, $nbRows = 0, $pageNumber = 1)
    {
        $value = '';
        if (isset($iterator)) {

            $iterator->seek(($pageNumber - 1) * $this->getPageLength());
            $record = $iterator->current();
            if ($record instanceof ORM_Record) {
                $sortField = self::getRecordFieldValue($record, $this->sortField);
                if ($sortField instanceof ORM_Record) {
                    $sortField = $sortField->getRecordTitle();
                }
                $pageStartValue = mb_convert_case($sortField, MB_CASE_UPPER);

                $iterator->seek(min($nbRows - 1, $pageNumber * $this->getPageLength() - 1));
                $record = $iterator->current();
                $sortField = self::getRecordFieldValue($record, $this->sortField);
                if ($sortField instanceof ORM_Record) {
                    $sortField = $sortField->getRecordTitle();
                }
                $pageEndValue = mb_convert_case($sortField, MB_CASE_UPPER);
                $value = sprintf(widget_translate('Page %d : %s to %s'), $pageNumber, $pageStartValue, $pageEndValue);
            }
        }

        return $value;
    }



    /**
     *
     * @param int $nbPages
     * @param int $currentPage
     * @param int $maxLinks
     * @return int[]
     */
    protected static function initPageNumbers($nbPages, $currentPage, $maxLinks)
    {
        $pageNumbers = array();

        $step = max(1, ($nbPages - 1) / $maxLinks);

        for ($i = 0; $i < $nbPages; $i += $step) {
            $pageNumber = round($i + 1);
            $pageNumbers[$pageNumber] = $pageNumber;
        }
        $pageNumbers[$currentPage + 1] =  $currentPage + 1;
        if ($currentPage < $nbPages - 1) {
            $pageNumbers[$currentPage + 2] = $currentPage + 2;
        }
        if ($currentPage > 0) {
            $pageNumbers[$currentPage] =  $currentPage;
        }
        sort($pageNumbers);

        return $pageNumbers;
    }


    protected function initRequest()
    {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $rewrittenFile = basename($url['path']);
        $realFile = basename($_SERVER['PHP_SELF']);

        if ($rewrittenFile !== $realFile) {
            $this->gp = array();
        } else {
            $this->gp = $_GET + $_POST;
        }

        $namePath = $this->getPageNamePath();
        $fieldname = array_pop($namePath);

        $this->pageNumberGp =& $this->gp;
        foreach ($namePath as $name) {
            if (!isset($this->pageNumberGp[$name])) {
                $this->pageNumberGp[$name] = array();
            }
            $this->pageNumberGp =& $this->pageNumberGp[$name];
        }
        $this->pageNumberGp[$fieldname] = null;
        $this->pageNumberGp =& $this->pageNumberGp[$fieldname];
    }

    /**
     * Pagination display.
     *
     * @param Widget_Canvas $canvas
     * @return mixed
     */
    protected function linksPageSelector(Widget_Canvas $canvas)
    {
        $W = bab_Widgets();
        $this->initRequest();

        $nbPages = $this->getNbPages();
        $currentPage = $this->getCurrentPage();

        $pageNumbers = $this->initPageNumbers($nbPages, $currentPage, $this->maxLinks);

        if (isset($this->sortField)) {
            $iterator = $this->iterator;
            $nbRows = $iterator->count();
        } else {
            $iterator = null;
            $nbRows = null;
        }

        $links = array();

        $ajaxClass = '';
        if ($this->ajaxUrl) {
            $ajaxClass = ' widget-ajax';
        }

        $u = 0;

        $classes = 'page-link' . $ajaxClass;

        // Previous page link
        if ($this->getCurrentPage() > 0) {

            $value = $this->getPageLabel($iterator, $nbRows, $currentPage);

            $url = $this->pageSelectorUrl($currentPage - 1);

            $lid = $this->getId() . '_item_' . $u;
            $u++;
            $metadata = $this->getMetadata();
            if (isset($metadata['ajaxAction'])) {
                $metadata['ajaxAction'] .= '&page=' . ($currentPage - 1);
            }
            $links[] = $W->Link('<', $url)
                ->setId($lid)
                ->setTitle($value)
                ->addClass($classes)
                ->setSizePolicy('page-item')
                ->setMetadata(null, $metadata);
        } else {
            $links[] = $W->Link('<')
                ->disable()
                ->addClass($classes)
                ->setSizePolicy('page-item disabled');
        }

        // Intermediate pages links
        foreach ($pageNumbers as $pageNumber) {

            $value = $this->getPageLabel($iterator, $nbRows, $pageNumber);

            $url = $this->pageSelectorUrl($pageNumber - 1);

            $active = '';
            if ($pageNumber == $currentPage + 1) {
                $active = ' active';
            }
            $lid = $this->getId() . '_item_' . $u;
            $u++;
            $metadata = $this->getMetadata();
            if (isset($metadata['ajaxAction'])) {
                $metadata['ajaxAction'] .= '&page=' . ($pageNumber - 1);
            }

            $links[] = $W->Link($pageNumber . '', $url)
                ->setId($lid)
                ->setTitle($value)
                ->addClass($classes)
                ->setSizePolicy('page-item' . $active)
                ->setMetadata(null, $metadata);
        }

        // Next page link
        if ($this->getCurrentPage() < $nbPages - 1) {

            $value = $this->getPageLabel($iterator, $nbRows, $currentPage + 2);

            $url = $this->pageSelectorUrl($currentPage + 1);

            $lid = $this->getId() . '_item_' . $u;
            $u++;
            $metadata = $this->getMetadata();
            if (isset($metadata['ajaxAction'])) {
                $metadata['ajaxAction'] .= '&page=' . ($currentPage + 1);
            }
            $links[] = $W->Link('>', $url)
                ->setId($lid)
                ->setTitle($value)
                ->addClass($classes)
                ->setSizePolicy('page-item')
                ->setMetadata(null, $metadata);
        } else {
            $links[] = $W->Link('>')
                ->disable()
                ->addClass($classes)
                ->setSizePolicy('page-item disabled');
        }

        $this->ajaxUrl = null;

        return $canvas->ul($this->getId(), $this->getClasses(), $links);
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if ($this->getNbPages() > 1) {

            return $this->linksPageSelector($canvas)
                . $canvas->metadata($this->getId(), $this->getMetadata());
        }

        return '';
    }
}
