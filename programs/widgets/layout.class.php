<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/item.class.php';

/**
 * Constructs a Widget_Layout.
 *
 * @param string $id	The item unique id.
 * @return Widget_Layout
 */
function Widget_Layout($id = null)
{
    return new Widget_Layout($id);
}

/**
 * The Widget_Layout class is the base class of geometry managers.
 *
 * A Widget_Layout object is used to specifiy the way items contained in a
 * {@link Widget_ContainerWidget} are laid out on the page.
 * A layout can contain {@link Widget_Widget} objects or other Widget_Layout objects
 * so that it is possible to combine several layouts to design a complex dialog.
 */
class Widget_Layout extends Widget_Item implements Widget_Displayable_Interface
{
    private $items;
    private $initScript;

    private $sortable		= false;
    private $connectWith	= false;
    private $sortableReload = false;
    private $sortableReloadReceive = false;

    const DEFAULT_SPACING = 8;

    const DEFAULT_VERTICAL_ALIGN = 'top';		// top, middle, bottom
    const DEFAULT_HORIZONTAL_ALIGN = 'left';	// left, center, right



    /**
     * @param string $id	The item unique id.
     * @return Widget_Layout
     */
    public function __construct($id = null)
    {
        assert('is_string($id) || is_null($id); /* The id parameter must be a string or null */');
        $this->items = array();
        parent::__construct($id);
        $this->setInitScript('');
        $this->canvasOptions = Widget_Item::Options();
    }

    public function setInitScript($script)
    {
        $this->initScript = $script;
    }


    /**
     * Sets the parent of the layout.
     *
     * @param Widget_ContainerWidget|Widget_Layout $parent
     */
    public function setParent($parent)
    {
        parent::setParent($parent);
        foreach (array_keys($this->items) as $key) {
            $item = $this->items[$key];
            $item->setParent($parent);
        }
    }



    /**
     * Adds $item to this layout in a manner specific to the layout.
     *
     * @param Widget_Displayable_Interface $item
     * @return $this
     */
    public function addItem(Widget_Displayable_Interface $item = null)
    {
        if ($item === null) {
            return $this;
        }
        assert('is_null($item->getParent()); /* The item must not already have a parent */');
        $this->items[$item->getId()] = $item;
        $item->setParent($this->getParent());
        return $this;
    }


    /**
     * Adds items to this layout in a manner specific to the layout.
     * Items can be an array of widget items, or multiple items in the arguments list.
     *
     * @param Widget_Item[] $items
     * @return $this
     */
    public function addItems($items)
    {
        if (! is_array($items)) {
            $items = func_get_args();
        }

        foreach ($items as $item) {
            $this->addItem($item);
        }
        return $this;
    }



    /**
     * remove $item to this layout in a manner specific to the layout.
     *
     * @param Widget_Displayable_Interface $item
     * @return $this
     */
    public function removeItem(Widget_Displayable_Interface $item = null)
    {
        if ($item === null) {
            return $this;
        }

		unset($this->items[$item->getId()]);
        $item->setParent(null);
        return $this;
    }


    /**
     * Sets whether the each field of the layout is mandatory or not.
     *
     * If the field is inside a dialog, the dialog will report an error to
     * the user if he did not fill in the field on submit.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param boolean $mandatory
     * @param string $message		The message displayed by the dialog if the field is left empty.
     * @return Widget_Widget
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        assert('is_bool($mandatory); /* The "mandatory" parameter must be a boolean. */');
        assert('is_string($message); /* The "message" parameter must be a string. */');

        $items = $this->getItems();
        foreach ($items as $item) {
            $item->setMandatory($mandatory, $message);
        }
        return $this;
    }



    /**
     * Returns an array of the items managed by the layout.
     *
     * @return array		An array of the items managed by the layout.
     */
    public function getItems()
    {
        return $this->items;
    }


    /**
     * Returns the item with identifier $id managed by the layout.
     *
     * @return Widget_Item
     */
    public function getItem($id)
    {
        return $this->items[$id];
    }


    /**
     * Sets the spacing between items in the layout.
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @param 	int		$spacing
     * @param	string	$unit
     * @param	int		$vspace
     * @param	string	$vunit
     * @return $this
     */
    public function setSpacing($spacing, $unit = null, $hspace = null, $hunit = null)
    {
        assert('is_numeric($spacing); /* The spacing parameter must be an integer */');
        $this->setVerticalSpacing($spacing, $unit);
        if (null === $hspace) {
            $hspace = $spacing;
            $hunit = $unit;
        }

        $this->setHorizontalSpacing($hspace, $hunit);

        return $this;
    }


    /**
     * Sets the horizontal spacing (in pixels) between items in the layout.
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @param int	$spacing
     * @return $this
     */
    public function setHorizontalSpacing($spacing, $unit = null)
    {
        assert('is_numeric($spacing); /* The spacing parameter must be an integer */');
        $this->canvasOptions->horizontalSpacing($spacing, $unit);
        return $this;
    }


    /**
     * Sets the vertical spacing (in pixels) between items in the layout.
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @param int	$spacing
     * @return $this
     */
    public function setVerticalSpacing($spacing, $unit = null)
    {
        assert('is_numeric($spacing); /* The spacing parameter must be an integer */');
        $this->canvasOptions->verticalSpacing($spacing, $unit);
        return $this;
    }




    /**
     * Return the horizontal spacing (in pixels) between items in the layout.
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @deprecated
     * @see Widget_Item::Options()
     *
     * @return int
     */
    public function getHorizontalSpacing()
    {
        return $this->canvasOptions->horizontalSpacing();
    }

    /**
     * Return the vertical spacing (in pixels) between items in the layout.
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @return int
     */
    public function getVerticalSpacing()
    {
        return $this->canvasOptions->verticalSpacing();
    }


    /**
     * Sets the vertical align property for layout items
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @param string	$align		"top", "middle" or "bottom"
     * @return $this
     */
    public function setVerticalAlign($align)
    {
        $this->canvasOptions->verticalAlign($align);
        return $this;
    }


    /**
      * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @return string
     */
    public function getVerticalAlign()
    {
        return $this->canvasOptions->verticalAlign();
    }


    /**
     * Sets the horizontal align property for layout items
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @param string	$align		"left", "center" or "right"
     * @return $this
     */
    public function setHorizontalAlign($align)
    {
        assert('in_array($align, array("left", "center", "right")); /* The align parameter must be "left", "center" or "right" */');
        $this->canvasOptions->horizontalAlign($align);
        return $this;
    }


    /**
     * Use Widget_Item::Options() to set spacing and alignment.
     *
     * @see Widget_Item::Options()
     *
     * @return string
     */
    public function getHorizontalAlign()
    {
        return $this->canvasOptions->horizontalAlign();
    }


    /**
     * Set sortable attribute, default true
     * @param	bool	[$param]
     *
     * @return	$this
     */
    public function sortable($param = true, $connectWith = null, $reloadOnRemove = false, $reloadOnReceive= false)
    {
        $this->sortable = $param;

        if($connectWith){
            $this->connectWith = $connectWith;
            $this->sortableReload = $reloadOnRemove;
            $this->sortableReloadReceive = $reloadOnReceive;
        }

        return $this;
    }


    /**
     * Dumps the item for debugging purpose.
     *
     * @param string	$prefix
     * @param bool		$standalone
     * @return string
     * @ignore
     */
    public function dump()
    {
        $dumpString = parent::dump();
        $dumpString .= '<ul>' . "\n";

        $items = $this->getItems();

        foreach ($items as $item) {
            $dumpString .= '<li>' . $item->dump() . '</li>' . "\n";
        }
        $dumpString .= '</ul>' . "\n";
        return $dumpString;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Item::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-layout';

        if ($this->sortable) {
            $classes[] = 'widget-sortable';
            if($this->connectWith){
                $connectWith = $this->connectWith;
                if (!is_array($connectWith)) {
                    $connectWith = array($connectWith);
                }

                foreach ($connectWith as $k => $v) {
                    if ($v instanceof Widget_Item) {
                        $connectWith[$k] = '#'.$v->getId();
                    } elseif(is_string($v)) {
                        $connectWith[$k] = $v;
                    }
                }
                $this->setMetadata('connectWith', $connectWith);
                $this->setMetadata('sortableReload', $this->sortableReload);
                $this->setMetadata('sortableReloadReceive', $this->sortableReloadReceive);
            }
        }

        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $items = array();
        foreach ($this->getItems() as $item) {
            $items[] = $item;
        }
        return $canvas->defaultLayout(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
