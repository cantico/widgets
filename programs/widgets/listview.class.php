<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once dirname(__FILE__) . '/flowlayout.class.php';



/**
 * Constructs a Widget_ListView.
 *
 * @param string		$id			The item unique id.
 * @return Widget_ListView
 */
function Widget_ListView($id = null)
{
	return new Widget_ListView($id);
}



/**
 * A Widget_ListView is a widget that displays a list of Widget_Icons.
 */
class Widget_ListView extends Widget_ContainerWidget implements Widget_Displayable_Interface
{
	private $viewType;

	const VIEW_ICONS = 'widget-view-as-icon';
	const VIEW_LIST = 'widget-view-as-list';
	const VIEW_DETAILS = 'widget-view-as-details';

	/**
	 * @param string $id			The item unique id.
	 * @return Widget_Listview
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->setLayout(new Widget_FlowLayout());
	}


	/**
	 * Sets how items are displayed in the list view.
	 *
	 * Possible values are:
	 *  - Widget_ListView::VIEW_ICONS
	 *  - Widget_ListView::VIEW_LIST
	 *  - Widget_ListView::VIEW_DETAILS
	 *
	 * @param string $viewType
	 * @return Widget_ListView
	 */
	public function setView($viewType)
	{
		$this->viewType = $viewType;
		return $this;
	}


	/**
	 * Returns the type of list view (VIEW_ICONS, VIEW_LIST, VIEW_DETAILS).
	 *
	 * @see Widget_ListView::setView
	 * @return string
	 */
	public function getView()
	{
		return $this->viewType;
	}



	
	function getClasses() {
		$classes = parent::getClasses();
		$classes[] = 'widget-listview';
		return $classes;
	}



	function display(Widget_Canvas $canvas)
	{
		$layout = $this->getLayout();
		return $canvas->div($this->getId(), $this->getClasses(), array($layout));
	}
}

