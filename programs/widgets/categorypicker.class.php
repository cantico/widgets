<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/drilldownmenu.class.php';


/**
 * Constructs a Widget_TopicPicker.
 *
 * @param string        $id         The item unique id.
 * @return Widget_TopicPicker
 */
function Widget_CategoryPicker($id = null)
{
    return new Widget_CategoryPicker($id);
}


/**
 * A Widget_TopicPicker is a widget that let the user select an ovidentia topic.
 *
 *
 */
class Widget_CategoryPicker extends Widget_DrilldownMenu implements Widget_Displayable_Interface
{

    /**
     * @param string $id            The item unique id.
     * @return Widget_TopicPicker
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
        $this->setDatasource(new bab_url('?tg=addon/widgets/category&idx=categorypicker'));
        $this->setMetadata('crumbDefaultText', widget_translate('Choose a category'));

    }




    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-categorypicker';

        return $classes;
    }



    public function display(Widget_Canvas $canvas)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
        $value = $this->getValue();
        if ($value) {
            $this->setTitle(bab_getCategoryTitle($value));
        } else {
            $this->setTitle(widget_translate('No category selected'));
        }

        return parent::display($canvas);
    }
}
