<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__).'/inputwidget.class.php';



/**
 * Constructs a Widget_Radio.
 *
 * @param string		$id			The item unique id.
 * @return Widget_Radio
 */
function Widget_Radio($id = null)
{
	return new Widget_Radio($id);
}


/**
 * A Widget_Radio
 * the widget can be used by adding a RadioSet in page and call the setOption method
 * or displaying the Widget_Radio individually and call the setRadioSet method
 *
 * @see Widget_RadioSet
 */
class Widget_Radio extends Widget_InputWidget implements Widget_Displayable_Interface
{
	/**
	 *  @var Widget_RadioSet $_radioSet The radio set to which this radio button is associated.
	 */
	private $_radioSet = null;


	private $_checkedValue = null;


	/**
	 * @param string $id                The item unique id.
	 * @param Widget_RadioSet $radioSet The radio set to which this radio button will be attached.
	 */
	public function __construct($id = null, Widget_RadioSet $radioSet = null)
	{
		parent::__construct($id);
		$this->setRadioSet($radioSet);
	}



	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-radio';
		return $classes;
	}

	/**
	 * Associates this radio button to a radio set.
	 *
	 * @return Widget_RadioSet
	 */
	public function setRadioSet(Widget_RadioSet $radioSet) {

		$this->_radioSet = $radioSet;
		
		if (isset($radioSet)) {
			$radioSet->attachRadio($this);
		}
		
		return $this;
	}

	
	public function setCheckedValue($value)
	{
		$this->_checkedValue = $value;
		return $this;
	}

	/**
	 * @return bool
	 */
	protected function isChecked()
	{
		return ((string) $this->_radioSet->getValue()) === ((string) $this->_checkedValue);
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getFullName()
	 */
	public function getFullName()
	{
		return $this->_radioSet->getFullName();
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		if (!isset($this->_radioSet)) {
			throw new Exception('missing Widget_RadioSet');
		}


		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			if ($this->isChecked()) {

				return $canvas->richtext(
						$this->getId(),
						$classes,
						$this->getValue(),
						BAB_HTML_ALL ^ BAB_HTML_P,
						$this->getCanvasOptions()
				);
			} else {
				return '';
			}


		} else {

			return $canvas->radioInput(
				$this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$this->_checkedValue,
				$this->isChecked(),
				$this->isDisabled()
			). $canvas->metadata($this->getId(), $this->getMetadata());
		}
	}
}
