<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/lineedit.class.php';



/**
 * A Widget_NumberEdit is a widget that lets the user enter a number.
 */
class Widget_NumberEdit extends Widget_LineEdit implements Widget_Displayable_Interface, Widget_LineEdit_Interface
{
    protected function getInputType()
    {
        return 'number';
    }
    
    public function setMin($num)
    {
        $this->addAttribute('min', $num);
        return $this;
    }
    
    public function setMax($num)
    {
        $this->addAttribute('max', $num);
        return $this;
    }
    
    public function setStep($step)
    {
        $this->addAttribute('step', $step);
        return $this;
    }
    
    
    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-numberedit';
        return $classes;
    }
}