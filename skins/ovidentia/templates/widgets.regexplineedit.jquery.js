



window.babAddonWidgets.regExpLineEditEvent = function() {


    var enable = function(input) {

        input.addClass('widget-regexplineedit-valid');
        input.removeClass('widget-regexplineedit-invalid');
    };

    var disable = function(input) {

        input.removeClass('widget-regexplineedit-valid');
        input.addClass('widget-regexplineedit-invalid');
    };



    var input = jQuery(this);

    var regExpPattern = window.babAddonWidgets.getMetadata(this.id).regExp;
    var regExp = new RegExp(regExpPattern);


    if ('' == input.val())
    {
        enable(input);
        return;
    }


    if (!input.val().match(regExp))
    {
        disable(input);
        return;
    }

    enable(input);
    return;
};



function widget_regExpLineEditInit(domNode) {

    jQuery(domNode).find('.widget-regexplineedit').each(function() {
        var input = jQuery(this);

        if (input.data('widgetevent'))
        {
            return;
        }

        input.data('widgetevent', true);


        // lock form validation if not a valid regular expression

        input.blur(window.babAddonWidgets.regExpLineEditEvent);
        input.keyup(window.babAddonWidgets.regExpLineEditEvent);

        input.blur();


        // form event

        if (form = input.closest('form')) {
            if (form.data('widget-regexplineedit-event')) {
                // if mutiple field of same type, prevent mutiple submit event
                return;
            }

            form.data('widget-regexplineedit-event', true);

            form.on('validate.widgets', function() {
                var input = jQuery(this).find('.widget-regexplineedit-invalid')[0];

                if (input) {
                    input.focus();
                    var message = window.babAddonWidgets.getMetadata(input.id).submitMessage;

                    if (window.babAddonWidgets.validatemandatory) {
                        form.addClass('widget-invalid');
                        alert(message);
                        return false;
                    }

                    if (window.babAddonWidgets.validate) {
                        var confirm = window.confirm(message);
                        if (!confirm) {
                            form.addClass('widget-invalid');
                        }
                        return confirm;
                    }
                }

                return true;
            });
        }

    });

}



window.bab.addInitFunction(widget_regExpLineEditInit);

