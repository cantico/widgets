





/**
 * Change visibility of columns with a css class
 * 
 * @param {DOMElement}  tableview	Widget dom element
 * @param {int} 		cellIndex
 * @param {String} 		visibility	visible : will show the olumn
 * 									hidden : will hide the column
 * 									toggle : will change the column visibility status
 * 
 * @return {bool} true if column is visible after call.
 */
window.babAddonWidgets.tableview_setColumnVisible = function(tableview, cellIndex, visibility) {

    var tab = tableview.getElementsByTagName('TABLE')[0];

    var colStart = 0;
    for (var c = 0; c < cellIndex; c++) {
        colStart += tab.rows[0].cells[c].colSpan;
    }

    var nbRows = tab.rows.length;
    var tdColSpan = tab.rows[0].cells[cellIndex].colSpan;
    var headerCell = jQuery(tab.rows[0].cells[colStart]);
    
    

    var visible;
    if (visibility === 'visible') {
        visible = true;
    } else if (visibility === 'hidden') {
        visible = false;
    } else {
        visible = headerCell.hasClass('widget-hidden-column');
    }
    
    for (var r = 0; r < nbRows; r++) {

        var row = tab.rows[r];
        for (c = 0; c < tdColSpan; c += row.cells[colStart + c].colSpan) {
        	var cell = jQuery(row.cells[colStart + c]);

            if (visible) {
            	cell.removeClass('widget-hidden-column');
            } else {
            	cell.addClass('widget-hidden-column');
            }
        }
    }
    return visible;
}

window.babAddonWidgets.tableview_pieChartDataLabel = function(lgtxt, lgtxt2, datavalue, pctvalue)
{
    if (pctvalue < 5) {
        return '';
    }
    lgtxt = lgtxt.length > 15 ? lgtxt.substr(0, 15) + '...' : lgtxt;
    return lgtxt + " " + lgtxt2 + "\n" + datavalue + " (" + (Math.round(pctvalue * 10) / 10) + "%)" ;
}

window.babAddonWidgets.tableview_barChartDataLabel = function(lgtxt, lgtxt2, datavalue, pctvalue, yPosBottom, yPosTop)
{
    if (yPosBottom - yPosTop < 10) {
        return '';
    }
    return datavalue;
}


window.babAddonWidgets.tableview_chartDataAnnotation = function(lgtxt, lgtxt2, datavalue, pctvalue)
{
    return lgtxt + " " + lgtxt2 + "\n" + datavalue + " (" + (Math.round(pctvalue * 10) / 10) + "%)" ;
}
	

window.babAddonWidgets.tableview_init = function(domNode) {
    
//    jQuery(domNode).find('.widget-tableview').each(function () {
//        
//        var tableView = this;
//        
//        jQuery(this).find('.widget-table-header .widget-tableview-column-menu a.column-toggle').not('.widget-column-toggle-init-done').each(function () {
//            jQuery(this).addClass('widget-column-toggle-init-done');
//            
//            jQuery(this).click(function() {
//                var meta = window.babAddonWidgets.getMetadata(this.id);
//                var columnName = meta.columnName;
//                var visible = window.babAddonWidgets.tableview_setColumnVisible(tableView, meta.cellIndex, 'toggle');
//                jQuery.ajax({
//                    url: '',
//                    data: 'tg=addon/widgets/configurationstorage&key='+encodeURIComponent(tableView.id + '/' + 'columns/' + columnName) + '&value=' + (visible ? '1' : '0')
//                });
//                
//                jQuery(this).toggleClass('actions-dialog-ok');
//                jQuery(this).toggleClass('widget-strong');
//                return false;
//            });
//        });
//        
//        jQuery(this).find('.widget-table-header .widget-tableview-column-menu a.columns-reset').not('.widget-column-reset-init-done').each(function () {
//            
//            jQuery(this).addClass('widget-column-reset-init-done');
//            
//            jQuery(this).click(function() {
//                jQuery.ajax({
//                    url: '',
//                    data: 'tg=addon/widgets/configurationstorage&idx=delete&path=' + encodeURIComponent(tableView.id),
//                    success: function() {
//                        document.location.reload();
//                    }
//                });
//                
//                return false;
//            });
//        });
//    
//    });
    
    
    // Graphs
    jQuery(domNode).find(".widget-tableview.widget-view-as-chart").not('.widget-view-as-chart-init-done').each(function() {
        jQuery(this).addClass('widget-view-as-chart-init-done');
        
        var tableview = jQuery(this);
        var canvasId = this.id + 'chart';
        
        var table = tableview.find('table');
        
        
        var fontFamilies = window.getComputedStyle(window.document.body, null).getPropertyValue('font-family').split(',');
        var fontFamily = "'" + fontFamilies[0].replace(/"/g, '') + "'";

        var size = tableview.width();
        var width = size;
        var height = size * 0.5;

        tableview.append('<canvas id="' + canvasId + '" height="' + height + '" width="' + width + '"></canvas>');
        
        var ctx = document.getElementById(canvasId).getContext("2d");
        
        var options = {
            responsive: true,
            highLight: true,
            animation:false,
            decimalSeparator : ",",
            thousandSeparator : " ",
            scaleFontFamily: fontFamily,
            inGraphDataShow: true,
            inGraphDataFontFamily: fontFamily,
            annotateDisplay: true,
            annotateFontFamily: fontFamily,
            annotateFontSize: 10,
            annotateLabel: '<%=window.babAddonWidgets.tableview_chartDataAnnotation(lgtxt, lgtxt2, datavalue, pctvalue)%>',
            pointLabelFontFamily: fontFamily,
            legend: true,
            legendFontFamily: fontFamily,
            mouseDownLeft: function(event,ctx,config,data,other) {
                if (other === null) {
                    return;
                }
                if (ctx.tpchart === 'Pie' || ctx.tpchart === 'Doughnut' || ctx.tpchart === 'PolarArea') {
                    if (typeof(ctx.links[other.v1]) != 'undefined') {
                        ctx.links[other.v1].click();
                    }
                } else {
                    if (typeof(ctx.links[other.v2 + '.' + other.v1]) != 'undefined') {
                        ctx.links[other.v2 + '.' + other.v1].click();
                    }
                }
            }
        };
        
        var generateChartDataFromTable;
        var generateChartDatasetFromTable;
        if (tableview.hasClass('widget-chart-data-series-in-columns')) {
            generateChartDataFromTable = window.babAddonWidgets.tableview.generateChartDataFromTable;
            generateChartDatasetFromTable = window.babAddonWidgets.tableview.generateReverseChartDatasetFromTable;
        } else {
            generateChartDataFromTable = window.babAddonWidgets.tableview.generateChartDataFromTable;
            generateChartDatasetFromTable = window.babAddonWidgets.tableview.generateChartDatasetFromTable;
        }
        
        var myNewChart;
        if (tableview.hasClass('widget-chart-pie')) {
            window.babAddonWidgets.tableview_chartLabel
            options.inGraphDataTmpl = "<%=window.babAddonWidgets.tableview_pieChartDataLabel(lgtxt, lgtxt2, datavalue, pctvalue)%>";
//            options.inGraphDataTmpl = "<%=v1.length > 10 ? v1.substr(0, 10) + '...' : v1%>\n<%=v2%> (<%=v6%>%)";
            options.segmentStrokeWidth = 1;
            //options.legend = false;
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).Pie(data, options);
        } else if (tableview.hasClass('widget-chart-doughnut')) {
            options.inGraphDataTmpl = "<%=window.babAddonWidgets.tableview_pieChartDataLabel(lgtxt, lgtxt2, datavalue, pctvalue)%>";
//            options.inGraphDataTmpl = "<%=v1.length > 10 ? v1.substr(0, 10) + '...' : v1%>\n<%=v2%> (<%=v6%>%)";
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).Doughnut(data, options);
        } else if (tableview.hasClass('widget-chart-polararea')) {
            options.inGraphDataTmpl = "<%=v1.length > 10 ? v1.substr(0, 10) + '...' : v1%>\n<%=v2%> (<%=v6%>%)";
            var data = generateChartDataFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).PolarArea(data, options);
        } else if (tableview.hasClass('widget-chart-radar')) {
            //options.inGraphDataTmpl = "<%=v2%>";
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).Radar(data, options);
        } else if (tableview.hasClass('widget-chart-bar')) {
            options.inGraphDataAlign = 'center';
            options.inGraphDataTmpl = "<%=window.babAddonWidgets.tableview_barChartDataLabel(lgtxt, lgtxt2, datavalue, pctvalue, yPosBottom, yPosTop)%>";
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).Bar(data, options);
        }  else if (tableview.hasClass('widget-chart-line')) {
            options.inGraphDataAlign = 'center';
            options.inGraphDataTmpl = "<%=v3%>";
            options.datasetFill = false;
            options.bezierCurve = true;
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).Line(data, options);
        } else { //if (jQuery(tableView).hasClass('widget-chart-stackedbar')) {
            options.inGraphDataAlign = 'center';
            options.inGraphDataTmpl = "<%=window.babAddonWidgets.tableview_barChartDataLabel(lgtxt, lgtxt2, datavalue, pctvalue, yPosBottom, yPosTop)%>";
            var data = generateChartDatasetFromTable(table);
            ctx.links = table.links;
            myNewChart = new Chart(ctx).StackedBar(data, options);
        }

        table.hide();
	});
};




window.babAddonWidgets.tableview = {
    
    getColorHue: function (max, index, opacity) {
        return 'hsla(' + ((360 / max) * index) + ', 30%, 70%, ' + opacity + ')';
    },
    
    getColorSaturation: function (max, index, opacity) {
        return 'hsla(0, 0%, ' + (10 + (90 / max) * index) + '%, ' + opacity + ')';
    },
        
    generateChartDataFromTable: function(table) {
        
        var colors = [];
    
        var rows = jQuery(table).find('tr');
        var nbRows = rows.length - 1;
        
        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var chartData = [];
        rows.each(function(index) {
            if (index != 0) {
                var cols = $(this).find('td');  
                var value;
            	var label;
            	var link;
                cols.each(function(innerIndex) {
                    if (innerIndex == 0) {
                    	label = jQuery(this).text().trim();
                	} else {
                        value = jQuery(this).text();
                        link = jQuery(this).find('a').get(0);
                        if (link) {
                            table.links[label] = link;
                        }
                    }
                });
                
                var getColor = window.babAddonWidgets.tableview.getColor;
    
                var data = {
                    color : getColor(nbRows, index - 1, 0.9),
                    highlight: getColor(nbRows, index - 1, 1),
                    label : label,
                    title : label,
                    value : value
                }
    
                chartData.push(data);
            }
        });
    
        return chartData;
    },
    	
    generateChartDatasetFromTable: function(table) {
    
        var rows = jQuery(table).find('tr');
        var nbRows = rows.length - 1;
        
        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var labels = [];
        jQuery(table).find('tr').first().find('td,th').each(function (index) {
            if (index != 0) {
                labels.push(jQuery(this).text().trim());                           
            }
        });
    
        var datasets = [];		
        rows.each(function(index) {
            if (index != 0) {
                var cols = jQuery(this).find('td');  
                var data = [];
                var label;
                var link;
                cols.each(function(innerIndex) {
                    if (innerIndex == 0) {
                        label = jQuery(this).text().trim();
                    } else {
                        data.push(jQuery(this).text());
                        link = jQuery(this).find('a').get(0);
                        if (link) {
                            table.links[labels[innerIndex - 1] + '.' + label] = link;
                        }
                    }
                });
                
                var getColor = window.babAddonWidgets.tableview.getColor;
    
                var dataset = {
                    fillColor: getColor(nbRows, index - 1, 0.5),
                    strokeColor: getColor(nbRows, index - 1, 1),
                    pointColor: getColor(nbRows, index - 1, 1),
                    pointStrokeColor: '#fff',
                    highlightFill : getColor(nbRows, index - 1, 0.75),
                    highlightStroke: getColor(nbRows, index - 1, 1),
                    title : label,
                    data : data
                }
    
                datasets.push(dataset);
            }
        });
    
        var chartData = {
            labels: labels,
            datasets: datasets
        };
        
        return chartData;
    },
    
    
    generateReverseChartDatasetFromTable: function(table) {

        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var labels = [];
        jQuery(table).find('tr').find('td:nth-child(1),th:nth-child(1)').each(function (index) {
            if (index != 0) {
                labels.push(jQuery(this).text().trim());
            }
        });
        
        var datasets = [];
        
        var cols = jQuery(table).find('tr').first().find('td,th');
        var nbCols = cols.length - 1;
        
        cols.each(function(index) {
            if (index === 0) {
                return;
            }
                       
            cellIndex = this.cellIndex;
            var data = [];
            var label;
            var link;
            rows = jQuery(this).closest('table').find('tr > td:nth-child(' + (cellIndex + 1) + '),tr > th:nth-child(' + (cellIndex + 1) + ')');
            rows.each(function(innerIndex) {
                if (innerIndex == 0) {
                    label = jQuery(this).text().trim();
                } else {
                    data.push(jQuery(this).text());
                    link = jQuery(this).find('a').get(0);
                    if (link) {
                        table.links[labels[innerIndex - 1] + '.' + label] = link;
                    }
                }
            });

            var getColor = window.babAddonWidgets.tableview.getColor;
            
            var dataset = {
                fillColor : getColor(nbCols, index - 1, 0.5),
                strokeColor: getColor(nbCols, index - 1, 1),
                highlightFill : getColor(nbCols, index - 1, 0.75),
                highlightStroke: getColor(nbCols, index - 1, 1),
                title : label,
                data : data
            }

            datasets.push(dataset);
        });
    
        var chartData = {
            labels: labels,
            datasets: datasets
        };
        
        return chartData;
    }
}

// We select the colorscheme
window.babAddonWidgets.tableview.getColor = window.babAddonWidgets.tableview.getColorHue;
//window.babAddonWidgets.tableview.getColor = window.babAddonWidgets.tableview.getColorSaturation;


window.bab.addInitFunction(window.babAddonWidgets.tableview_init);
