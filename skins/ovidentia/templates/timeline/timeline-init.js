
function widget_getScriptRelativePath()
{
    var scripts = document.getElementsByTagName('script');
    for (var i = 0; i < scripts.length; i++) {
        if (-1 != scripts[i].src.indexOf('timeline-init.js')) {
            var result = '';
            arr = scripts[i].src.split('/');
            for (var j in arr) {
                if ('timeline-init.js' != arr[j]) {
                    result+= arr[j]+'/';
                }
            }
            return result;
        }
    }
    return '';
}

Timeline_ajax_url = widget_getScriptRelativePath() + "timeline_ajax/simile-ajax-api.js";
Timeline_urlPrefix = widget_getScriptRelativePath() + "timeline_js/";
Timeline_parameters = "bundle=true";
