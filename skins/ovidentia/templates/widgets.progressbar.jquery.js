window.babAddonWidgets.progressBar = function(widget_id, progress, title, memory_usage) {
	
	jQuery('#'+widget_id+'_progress').width(progress+'%');
	
	if (null !== title) {
		window.babAddonWidgets.progressBarSetTitle(widget_id, title);
	}
	
	memnode = jQuery('#'+widget_id+'_memory').get(0);
	if (null != memnode) {
		while (memnode.firstChild) {
			memnode.removeChild(memnode.firstChild);
		}
		
		memnode.appendChild(document.createTextNode(memory_usage));
	}
	

	completedAction = window.babAddonWidgets.getMetadata(widget_id).completedAction;
	completedButton = window.babAddonWidgets.getMetadata(widget_id).completedButton;
	
	button = jQuery('#'+completedButton);
	
	
	
	if (100 != progress) {
		
		if (null != button && !button.attr('disabled')) {
			button.attr('disabled', 'disabled');
		}
		return;
	}

	if (null != completedButton) {
		window.babAddonWidgets.progressBarUnlock(widget_id);
		return;
	}
	
		
	setTimeout(function() {
		
		document.location.href = completedAction;
	}, 1000);
	
}



window.babAddonWidgets.progressBarUnlock = function(widget_id, title) {
	
	if (null != title) {
		window.babAddonWidgets.progressBarSetTitle(widget_id, title);
	}
	
	
	completedAction = window.babAddonWidgets.getMetadata(widget_id).completedAction;
	completedButton = window.babAddonWidgets.getMetadata(widget_id).completedButton;
	
	
	if (null == completedAction) {
		return;
	}
	
	if (null != completedButton) {
	
		setTimeout(function() {
		
			button = jQuery('#'+completedButton);
			button.removeAttr('disabled');
			button.click(function() {
				document.location.href = completedAction;
			});
		}, 1300);
	
		return;
	}
	
	alert('progressBar : missing button');
}







window.babAddonWidgets.progressBarSetTitle = function(widget_id, title) {

	titlenode = jQuery('#'+widget_id+'_title').get(0);
	
	if (null != titlenode) {
		while (titlenode.firstChild) {
			titlenode.removeChild(titlenode.firstChild);
		}
		
		titlenode.appendChild(document.createTextNode(title));
	}
}






window.babAddonWidgets.progressBarMonitor = function(frame) {
	
	var Content = frame.contents().find('body > *').not("script");
	var widget = frame.closest('div');

	if (Content.length && '' != Content.text()) {
		
		
		
		var error = widget.find('.widget-progressbar-error');
		
		if (!error.is('div')) {
			var error = jQuery('<div class="widget-progressbar-error"></div>');
			widget.append(error);
		}
		
		// moving the node does not work on all browser, inserting a clone work better
		
		error.append(Content.clone());
		Content.remove();
	}
	
	setTimeout(function() {
		window.babAddonWidgets.progressBarMonitor(frame);
	}, 1000);
}









function widget_progressBarInit(domNode)
{
	jQuery(domNode).find('.widget-progressbar iframe').each(function() {
		window.babAddonWidgets.progressBarMonitor(jQuery(this));
	});
	
	
	// start process
	jQuery(domNode).find('.widget-progressbar').not('.widget-init-done').each(function() {
		var progressBar = jQuery(this);
		progressBar.addClass('widget-init-done');
		
		var processUrl = window.babAddonWidgets.getMetadata(progressBar.attr('id')).progressAction;
		
		progressBar.find('iframe').attr('src', processUrl);
		
	});
}



window.bab.addInitFunction(widget_progressBarInit);


