
function widget_babFilePickerInit(domNode)
{	
	jQuery(domNode).find('.widget-babfilepicker').not('.widget-init-done').each(function() {
		
		jQuery(this).addClass('widget-init-done');

		var babFilePicker = jQuery(this);

		var babFilePickerLineEdit = babFilePicker.children().get(0);
		jQuery(this).append('<input type="hidden" name="' + babFilePickerLineEdit.name + '" value="' + babFilePickerLineEdit.value + '" />');
		
		jQuery(babFilePickerLineEdit).hide();

		var text = window.babAddonWidgets.getMetadata(babFilePickerLineEdit.id).babFolderName;
		jQuery(this).append('<div>' + text + '</div>');
		babFilePickerLineEdit.name = '';
		
		
		jQuery(this).click(function() {
			
			var babFilePickerHidden = babFilePicker.children().get(1);
			var babFilePickerLabel = babFilePicker.children().get(2);
			

			bab_dialog.selectfile(
				function (params) {
					for (var i = 0; i < params.length; i++) {
						param = params[i];
						
						
						var pathElements = param.id.split(':');
						
						pathElements.shift();
//						pathElements.unshift('DG0');
						var path = pathElements.join('/');

						
						
						jQuery(babFilePickerLabel).text(path);
						jQuery(babFilePickerHidden).val('DG0/' + path);
						break;
					} 
					//
					//babFilePickerHidden.val(param['id']);
				},
				'show_collective_folders&show_sub_folders&show_personal_folders&selectable_collective_folders&selectable_sub_folders&multi'
			);

		});

	});

}


window.bab.addInitFunction(widget_babFilePickerInit);
